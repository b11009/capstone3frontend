import { Fragment, useEffect, useState } from 'react';
import ProductEditor from '../components/ProductEditor';
import { Container, Row, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';
import './UpdateProduct.css';


export default function Products() {
	const [allProducts, setAllProducts] = useState([]);

	const modifyStatus = (id , name, newStatus)=>{
		console.log(id, newStatus)
		fetch(`https://glacial-earth-62899.herokuapp.com/products/${id}/${newStatus}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			Swal.fire({
					title: `Product ${newStatus}d`,
					icon: "success",
					text: `${name} has been ${newStatus}d`
				})
			fetch('https://glacial-earth-62899.herokuapp.com/products/all')
				.then(res => res.json())
				.then(data => {
					console.log({data});
					refreshEditor(data)
				})

		})
	}

	const updateProduct = (id, editProductName, editProductDescription, editProductPrice, editProductCategory, editProductImgUrl) => {
  		console.log(
			{
			    "name" : editProductName,
			    "description" : editProductDescription,
			    "price" : editProductPrice,
			    "imgSrc": editProductImgUrl,
			    "category" : editProductCategory
			}
		)
  		fetch(`https://glacial-earth-62899.herokuapp.com/products/${id}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify(
			    {
				    "name" : editProductName,
				    "description" : editProductDescription,
				    "price" : editProductPrice,
				    "imgSrc": editProductImgUrl,
				    "category" : editProductCategory
				}
			)
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data){
				Swal.fire({
					title: "Update Success",
					icon: "success",
					text: `${editProductName} has been updated`
				})
				fetch('https://glacial-earth-62899.herokuapp.com/products/all')
				.then(res => res.json())
				.then(data => {
					console.log({data});
					refreshEditor(data)
				})
			}
			else{
				Swal.fire({
					title: "Failed Product Update",
					icon: "error",
					text: "Please try again."
				})
			}
		})
  	}


	const refreshEditor = (data) =>{
		setAllProducts(
			<Container class="products">
				<h1 className="productMenu">All Products</h1>
				<Row lg={3} md={4} sm={12} xs={12} className="productList g-2">
				   	{data.map((product,idx) => {
				        return (<ProductEditor key={product._id} editProductProp={product} modifyStatus={modifyStatus} updateProduct={updateProduct}/>
				       	)
				    })}
				</Row>
			</Container>
		)
	}

	// Create a function to fetch database
	useEffect(() => {
		fetch('https://glacial-earth-62899.herokuapp.com/products/all')
		.then(res => res.json())
		.then(data => {
			console.log({data});
			refreshEditor(data)
			})
	}, []);


	return(
		<Fragment>
			{allProducts}
		</Fragment>
	)
}