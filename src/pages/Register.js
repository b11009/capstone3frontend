import { useState, useEffect, useContext, Fragment } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { Form, Button, Row, Col  } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import './Register.css';

export default function Register(){

	const {user} = useContext(UserContext);
	const history = useNavigate();

	// State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);
	
	function registerUser(e){
		e.preventDefault();

			fetch('https://glacial-earth-62899.herokuapp.com/users/register', {
		                    method: "POST",
		                    headers: {
		                        'Content-Type' : 'application/json'
		                    },
		                    body: JSON.stringify({
		                    	firstName: firstName,
		                    	lastName: lastName,
		                    	mobileNo: mobileNo,
		                        email: email,
		                        password: password1
		                    })
		                })
		                .then(res => res.json())
		                .then(data => {
		                    console.log(data);

		                    if (data.message){
		                    	Swal.fire({
		                            title: 'Email Already used',
		                            icon: 'error',
		                            text: 'Please try again.'   
		                        });
		                    } else if (data === true) {
		                        Swal.fire({
		                            title: 'Registration successful',
		                            icon: 'success',
		                            text: 'Welcome to Pet Store!'
		                        });
		                        history("/login");
		                    } else {
		                        Swal.fire({
		                            title: 'Registration Failed',
		                            icon: 'error',
		                            text: 'Please try again.'   
		                        });
		                    }
		                })
		            setEmail('');
					setPassword1('');
					setPassword2('');
		         }         

	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both passwords match
		if((firstName !== '' && lastName !== '' && mobileNo.length === 11 && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [firstName, lastName, mobileNo, email, password1, password2]);

	return (
		(user.id !== null) ?
			<Navigate to="/" />
		:

		<Fragment>
			<div class="header">
				<h1 class="py-2">Register</h1>
			</div>

			<div className="justify-content-center d-block">
				<Form  onSubmit={(e) => registerUser(e)}>
				  <Form.Group as={Row} className="mb-3" controlId="firstName" >
				    <Form.Label column sm={2} className= "label" >First Name</Form.Label>
				     <Col sm={10}>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Enter First Name" 
				    	value={firstName}
				    	onChange={e => setFirstName(e.target.value)}
				    	required
				    />
				    </Col>
				  </Form.Group>

				  <Form.Group as={Row} className="mb-3" controlId="lastName">
				    <Form.Label column sm={2} className= "label" >Last Name</Form.Label>
				     <Col sm={10}>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Enter Last Name" 
				    	value={lastName}
				    	onChange={e => setLastName(e.target.value)}
				    	required
				    />
				    </Col>
				  </Form.Group>

				  <Form.Group as={Row} className="mb-3" controlId="mobileNo">
				    <Form.Label column sm={2} className= "label">Mobile Number</Form.Label>
				     <Col sm={10}>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Enter Mobile Number" 
				    	value={mobileNo}
				    	onChange={e => setMobileNo(e.target.value)}
				    	required
				    />
				    </Col>
				  </Form.Group>
				  <Form.Group as={Row} className="mb-3" controlId="userEmail">
				    <Form.Label column sm={2} className= "label">Email address</Form.Label>
				     <Col sm={10}>
				    <Form.Control 
				    	type="email" 
				    	placeholder="Enter email" 
				    	value={email}
				    	onChange={e => setEmail(e.target.value)}
				    	required
				    />
				    <Form.Text className="text-muted">
				      We'll never share your email with anyone else.
				    </Form.Text>
				    </Col>
				    
				  </Form.Group>

				  <Form.Group as={Row} className="mb-3" controlId="password1">
				    <Form.Label column sm={2} className= "label">Password</Form.Label>
				    <Col sm={10}>
				    <Form.Control 
				    	type="password" 
				    	placeholder="Password" 
				    	value={password1}
				    	onChange={e => setPassword1(e.target.value)}
				    	required
				    />
				    </Col>
				  </Form.Group>
				  
				  <Form.Group as={Row} className="mb-3" controlId="password2">
				    <Form.Label column sm={2} className= "label">Verify Password</Form.Label>
				     <Col sm={10}>
				    <Form.Control 
				    	type="password" 
				    	placeholder="Verify Password" 
				    	value={password2}
				    	onChange={e => setPassword2(e.target.value)}
				    	required
				    />
				    </Col>
				  </Form.Group>
				 
				 <Form.Group as={Row} className="mb-3">
				 <Col sm={{ span: 10, offset: 2 }}>	
				  { isActive ?
				  		<Button variant="primary" type="submit" id="submitBtn">
				  		  Submit
				  		</Button>
				  	:
					  	<Button variant="danger" type="submit" id="submitBtn" disabled>
					  	  Submit
					  	</Button>
				  }
				  </Col>
				  </Form.Group>
				</Form>
			</div>
		</Fragment>
	)
}