import { Fragment, useEffect, useState } from 'react';
import AdminOrderView from '../components/AdminOrderView';


export default function AllOrders () {

	const [myOrder, setMyOrder] = useState([]);

	useEffect(() => {
		console.log('getting orders')
		fetch('https://glacial-earth-62899.herokuapp.com/users/orders',
			 {
                method: "GET",
                headers: {
                   Authorization: `Bearer ${localStorage.getItem('token')}`
                }
         })
		.then(res => res.json())
		.then(data => {
			fetch('https://glacial-earth-62899.herokuapp.com/users/all',
			 {
                method: "GET",
                headers: {
                   Authorization: `Bearer ${localStorage.getItem('token')}`
                }
	         })
			.then(res => res.json())
			.then(userData => {
				console.log({userData});
				setMyOrder(data.map(checkout =>{
					return (<AdminOrderView key={checkout._id} order={checkout.order} 
						address={checkout.address} purchasedOn={checkout.purchasedOn}
						userId={checkout.userId} userData={userData.find(user=> user._id == checkout.userId)}
						/>)
				}
				))
				})
			})
	}, []);

	return(
		<Fragment>
			{myOrder}
		</Fragment>
	)
}