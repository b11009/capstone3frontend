import { useState, useEffect, useContext, Fragment } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { Form, Button, Col, Row } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import './CreateProduct.css';

export default function Products(){

	const {user} = useContext(UserContext);
	// const history = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [category, setCategory] = useState('');
	const [imgSrc, setImgSrc] = useState('');

	const [createdOn, setCreatedOn] = useState(new Date());
	const [isActive, setIsActive] = useState(true);

	function createProduct(e){

		e.preventDefault();

			const newProduct ={
		                    	name: name,
		                    	description: description,
		                    	price: price,
		                    	category: category
		                    }
		    if (imgSrc){
		    	newProduct.imgSrc = imgSrc
		    }

			console.log(user.token )
			fetch('https://glacial-earth-62899.herokuapp.com/products', {
		                    method: "POST",
		                    headers: {
		                        'Content-Type' : 'application/json',
								Authorization: `Bearer ${localStorage.getItem('token')}`
		                    },
		                    body: JSON.stringify(newProduct)
		                })
		                .then(res => res.json())
		                .then(data => {
		                    console.log(data);

		                    if (data.message){
		                    	Swal.fire({
		                            title: 'Product is already in the list',
		                            icon: 'error',
		                            text: 'Please try again.'   
		                        });
		                    } else if (data === true) {
		                        Swal.fire({
		                            title: 'You created new product',
		                            icon: 'success',
		                            text: 'Welcome to Pet Store!'
		                        });
		                    } else {
		                        Swal.fire({
		                            title: 'Product creation failed',
		                            icon: 'error',
		                            text: 'Please try again.'   
		                        });
		                    }
		                })
		            setName('');
					setDescription('');
					setPrice('');
					setCategory('');
					setImgSrc('');
		         }         

	useEffect(() => {
		if(name !== '' && description !== '' && price !== '' && category !== ''){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}

	}, [name, description, price, category]);

	return ( 
		<Fragment>
			<div className="header p-5">
				<h1>Create a new product</h1>
			</div>

			<div>
				<Form className="justify-content-center d-block" onSubmit={(e) => createProduct(e)}>
				  <Form.Group as={Row} className="mb-3" controlId="name">
				    <Form.Label column sm={2} className="label">Product Name</Form.Label>
				    <Col sm={10}>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Enter Product Name" 
				    	value={name}
				    	onChange={e => setName(e.target.value)}
				    	required
				    />
				    </Col>
				  </Form.Group>

				  <Form.Group as={Row} className="mb-3" controlId="description">
				    <Form.Label column sm={2} className="label">Description</Form.Label>
				    <Col sm={10}>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Enter Product Description" 
				    	value={description}
				    	onChange={e => setDescription(e.target.value)}
				    	required
				    />
				    </Col>
				  </Form.Group>

				  <Form.Group as={Row} className="mb-3" controlId="price">
				    <Form.Label column sm={2} className="label">Price</Form.Label>
				    <Col sm={10}>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Enter Product Price" 
				    	value={price}
				    	onChange={e => setPrice(e.target.value)}
				    	required
				    />
				    </Col>
				  </Form.Group>

				  <Form.Group as={Row} className="mb-3" controlId="category">
				    <Form.Label column sm={2} className="label" >Category</Form.Label>
				    <Col sm={10}>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Enter Product Category" 
				    	value={category}
				    	onChange={e => setCategory(e.target.value)}
				    	required
				    />
				    </Col>
				  </Form.Group>

				  <Form.Group as={Row} className="mb-3" controlId="imgSrc">
				    <Form.Label column sm={2} className="label">Image URL</Form.Label>
				    <Col sm={10}>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Enter URL" 
				    	value={imgSrc}
				    	onChange={e => setImgSrc(e.target.value)}
				    />
				    </Col>
				  </Form.Group>

				  { isActive ?
				  		<Button variant="primary" type="submit" id="submitBtn">
				  		  Submit
				  		</Button>
				  	:
					  	<Button variant="danger" type="submit" id="submitBtn" disabled>
					  	  Submit
					  	</Button>
				  }
				</Form>
			</div>
	</Fragment>
)
}