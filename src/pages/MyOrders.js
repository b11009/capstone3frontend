import { Fragment, useEffect, useState } from 'react';
import Order from '../components/Order';


export default function Orders () {

	const [myOrder, setMyOrder] = useState([]);

	useEffect(() => {
		console.log('getting orders')
		fetch('https://glacial-earth-62899.herokuapp.com/users/myOrders',
			 {
                method: "GET",
                headers: {
                   Authorization: `Bearer ${localStorage.getItem('token')}`
                }
         })
		.then(res => res.json())
		.then(data => {
			console.log({data});
			setMyOrder(data.map(checkout =>{
				return (<Order key={checkout._id} order={checkout.order} address={checkout.address} purchasedOn={checkout.purchasedOn}/>)
			}
			))
			})
	}, []);

	return(
		<Fragment>
			{myOrder}
		</Fragment>
	)
}