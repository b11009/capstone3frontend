import { Fragment, useEffect, useState } from 'react';
import ProductPreview from '../components/ProductPreview';
import { Row, Container, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ProductCarousel from '../components/ProductCarousel';
import PropTypes from 'prop-types';
import './Products.css';

export default function Products() {
	const [products, setProducts] = useState([]);
	const [featuredProduct, setFeaturedProduct] = useState([]);

	useEffect(() => {
		fetch('https://glacial-earth-62899.herokuapp.com/products')
		.then(res => res.json())
		.then(data => {
			console.log({data});
			fetch('https://glacial-earth-62899.herokuapp.com/users/hotproducts')
					.then(res => res.json())
					.then(hotProducts => {
						console.log({hotProducts});

					hotProducts.map(hp => {
						const match = data.find(p=> p._id === hp.productId);
						hp.description = match.description
						hp.imgSrc = match.imgSrc
						return hp
					})
					console.log("updated hotProducts", hotProducts)
					setFeaturedProduct(<ProductCarousel productProp={hotProducts.slice(0,3)} />)

					setProducts(
						<Container class="products">
							<h1 className="productMenu">All Products</h1>
							<Row lg={3} md={4} sm={12} xs={12} className="productList g-2">
							      {data.map((product,idx) => {
							        return (<ProductPreview key={product._id} productProp={product}/>)
							      })}
					   		</Row>
					   	</Container>
						)
					})
			}, []);
	}, []);


	return(
		<Fragment>
			{featuredProduct}
			{products}
		</Fragment>
	)
}