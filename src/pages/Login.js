import { useState, useEffect, useContext, Fragment } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

    const { user, setUser } = useContext(UserContext);
        
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState(''); 
    const [isActive, setIsActive] = useState(true);
		
    function authenticate(e) {

        e.preventDefault(); 
            fetch ('https://glacial-earth-62899.herokuapp.com/users/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)
            if(typeof data.access !== "undefined"){
                    // The JWT will be used to retrieve user information
                    localStorage.setItem('token', data.access);
                    retrieveUserDetails(data.access);
                    // Sweet alert message
                    Swal.fire({
                        title: "Login Successful",
                        icon: "success",
                        text: "You can shop now!"
                    })

                } else {
                    Swal.fire({
                        title: "Authentication Failed",
                        icon: "error",
                        text: "Check your login details and try again."
                    })
                }
        })
        setEmail('');
        setPassword('');
    }

    const retrieveUserDetails = (token) => {
            fetch('https://glacial-earth-62899.herokuapp.com/users/details', {
                headers: {
                    Authorization: `Bearer ${ token }`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                setUser({
                    email: email,
                    id: data._id,
                    isAdmin:  data.isAdmin
                    })
            })
        }


        useEffect(() => {
            if(email !== '' && password !== ''){
                setIsActive(true);
            }else{
                setIsActive(false);
            }

        }, [email, password]);

    return (
        (user.id !== null)?
            <Navigate to="/products" />
        :
        <Fragment>
        <div class="header">
            <h1> Login </h1>
        </div>

        <div className="justify-content-center d-block">
        <Form onSubmit={(e) => authenticate(e)}>
        
            <Form.Group as={Row} className="mb-3" controlId="email">
                <Form.Label column sm={2} className= "label" >Email address</Form.Label>
                <Col sm={10}>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
                </Col>
            </Form.Group>

            <Form.Group as={Row} className="mb-3" controlId="password">
                <Form.Label column sm={2} className= "label" >Password</Form.Label>
                <Col sm={10}>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
                </Col>
            </Form.Group>
            { isActive ? 
                <Button variant="primary" type="submit" id="loginBtn">
                    Submit
                </Button>

                : 
                <Button variant="danger" type="submit" id="loginBtn" disabled>
                    Submit
                </Button>
            }
        </Form>
        </div>
    </Fragment>
    )
}