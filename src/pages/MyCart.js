import { Container, Table ,Button} from 'react-bootstrap';
import { Fragment, useEffect, useContext, useState } from 'react';
import { Navigate, useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

import Cart from '../components/Cart';

// import UserOrder from '../components/UserOrder';

	
export default function MyCart() {

	const { user } = useContext(UserContext);
	const history = useNavigate();
	 	
	const updateCart = (productId , step) => {
		if (user.id !== null) {

	 	console.log("updateCart")
	 	console.log({productId})
	 	console.log({step})
	 	console.log(localStorage.getItem('token'))
		 	fetch('https://glacial-earth-62899.herokuapp.com/users/addToCart', {
		 			method: "POST",
		 			headers: {
		 				"Content-Type": "application/json",
		 				Authorization: `Bearer ${localStorage.getItem('token')}`
		 			},
		 			body: JSON.stringify([{
		 				productId: productId,
		 				quantity: step
		 			}])
		 	})
		 	.then(res => res.json())
		 	.then(data => {
		 		console.log("addToCart",data);
		 			if(data){
		 				Swal.fire({
		 					title: "Updated Cart",
		 					icon: "success",
		 					text: "You have successfully updated product quantity."
		 				})
						refreshCart(data)
		 			}
		 			else{
		 				Swal.fire({
		 					title: "Something went wrong",
		 					icon: "error",
		 					text: "Please try again."
		 				})
		 			}
		 	})
		 } else {
		 	history("/login");
		 }
	}   

	const refreshCart = ({newCart})=>{
		setMyCart(<Cart key={newCart._id} cart={newCart} updateCart={updateCart}/>)
	}

	const [myCart, setMyCart] = useState("");

	const checkoutCart = (e, cart, address) => {
		e.preventDefault()
		console.log('checking out cart')
		console.log(cart)
		console.log("checkout", 'test address')
		if (user.id !== null) {
			fetch('https://glacial-earth-62899.herokuapp.com/users/checkout', {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    "cart": cart,
				    "address": address
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				if(data){
					Swal.fire({
						title: "Check out orders",
						icon: "success",
						text: "Check out order Success"
					})
					history("/myOrder");
				}
				else{
					Swal.fire({
						title: "Failed Checkout Order",
						icon: "error",
						text: "Please try again."
					})
				}
			})
		} else {
			Swal.fire({
						title: "Please log in to order",
						icon: "error",
						text: "Only logged in users can order products"
					})
			history("/login");
		}
	}

	useEffect(() => {
		fetch('https://glacial-earth-62899.herokuapp.com/users/myCart', {
                method: "GET",
                headers: {
                   Authorization: `Bearer ${localStorage.getItem('token')}`
                }
         })
		.then(res => res.json())
		.then(data => {
			setMyCart(<Cart key={data._id} cart={data} updateCart={updateCart} checkoutCart={checkoutCart}/>)
			})
	}, []);

	return (<Fragment>
			{myCart}
			</Fragment>
	)       
}