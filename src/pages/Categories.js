import { Fragment, useEffect, useState } from 'react';
import ProductPreview from '../components/ProductPreview';
import { Row, Container, Col} from 'react-bootstrap';
import { Link ,useParams} from 'react-router-dom';
import PropTypes from 'prop-types';
import './Products.css';

export default function Products() {
	const [products, setProducts] = useState([]);
	const [productData, setProductData] = useState([]);
	const { categoryView } = useParams();
	console.log({categoryView})

	const [displayedCategory, setdisplayedCategory] = useState([]);


	const redraw = () =>{
		setdisplayedCategory(categoryView)
		setProducts(
				<Container class="products">
					<h1 className="productMenu">Products</h1>
					<Row lg={3} md={4} sm={12} xs={12} className="productList g-2">
					      {productData.filter(({category})=> categoryView && category
									      	? categoryView === category.toLowerCase().replace(/ /g, "") 
									      	: true )
					      		.map((product,idx) => {
					        return (<ProductPreview key={product._id} productProp={product}/>)
					      })}
			   		</Row>
			   	</Container>
		)

	}

	if (displayedCategory !== categoryView){redraw()}

	useEffect(() => {
		fetch('https://glacial-earth-62899.herokuapp.com/products')
		.then(res => res.json())
		.then(data => {
			console.log({data});
			setProductData(data)
			console.log("Cat Treats".toLowerCase().replace(/ /g, "") )
			setdisplayedCategory(categoryView)
			setProducts(
				<Container class="products">
					<h1 className="productMenu">Products</h1>
					<Row lg={3} md={4} sm={12} xs={12} className="productList g-2">
					      {data.filter(({category})=> categoryView && category
									      	? categoryView === category.toLowerCase().replace(/ /g, "") 
									      	: true )
					      		.map((product,idx) => {
					        return (<ProductPreview key={product._id} productProp={product}/>)
					      })}
			   		</Row>
			   	</Container>
				)
			})
	}, []);


	return(
		<Fragment>
			{products}
		</Fragment>
	)
}