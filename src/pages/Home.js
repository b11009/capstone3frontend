import { Fragment } from 'react';
import Banner from '../components/Banner';

export default function Home(){
	const data = {
	    title: "A paradise for a pet lover",
	    content: "Why not give the best to the ones you love the most?",
	    destination: "/products",
	    label: "Shop Now!"
	}

	return (
		<Fragment>
			<Banner data={data}/>
		</Fragment>
	)
}