import './App.css';
import { UserProvider } from './UserContext';
import { useState, useEffect } from 'react';
import { Route, Routes } from 'react-router-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Container } from 'react-bootstrap';

import Home from './pages/Home';
import Products from './pages/Products';
import Categories from './pages/Categories';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Register from './pages/Register';
import ProductPreview from './components/ProductPreview';
import ProductDetails from './components/ProductDetails';
import AppNavbar from './components/AppNavbar';
import Cart from './pages/MyCart';
import Order from './pages/MyOrders';
import AllOrders from './pages/AllOrders';
import CreateProduct from './pages/CreateProducts';
import UpdateProduct from './pages/UpdateProduct';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
          <Container>
            <Routes>
                  <Route path="/" element={<Home />} />
                  <Route path="/products" element={<Products />} />
                  <Route path="/products/category/:categoryView" element={<Categories />} />
                  <Route path="/createProduct" element={<CreateProduct />} />
                  <Route path="/products/details/:productId" element={<ProductDetails />} />
                  <Route path="/updateProduct" element={<UpdateProduct/>} />
                  <Route path="/login" element={<Login />} />
                  <Route path="/register" element={<Register />} />
                  <Route path="/logout" element={<Logout/>} />
                  <Route path="/myCart" element={<Cart/>} />
                  <Route path="/myOrder" element={<Order/>} />
                  <Route path="/viewAllOrders" element={<AllOrders/>} />
                  <Route path="*" element={<Error />} />
            </Routes>
          </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
