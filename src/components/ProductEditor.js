import { Row, Col, Card ,Button, Modal, Container} from 'react-bootstrap';
import { Fragment, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';
import Swal from 'sweetalert2';

export default function ProductEditor ({editProductProp, modifyStatus, updateProduct}) {

	const [allProducts, setAllProducts] = useState([]);
	const [editFormShow, setEditFormShow] = useState(false);
	const [editProductName, setEditProductName]= useState("");
	const [editProductDescription, setEditProductDescription]= useState("");
	const [editProductPrice, setEditProductPrice]= useState("");
	const [editProductCategory, setEditProductCategory]= useState("");
	const [editProductImgUrl, setEditProductImgUrl]= useState("");

	const handleClose = () => setEditFormShow(false);
  	const handleShow = (editProduct) => {
  		console.log('editProduct', editProduct)

  		setEditProductName(editProduct.name)
  		setEditProductDescription(editProduct.description)
  		setEditProductPrice(editProduct.price)
  		setEditProductImgUrl(editProduct.imgSrc)
  		setEditProductCategory(editProduct.category)
  		setEditFormShow(true)
  	};

  	const save = (id)=>{
  		setEditFormShow(false)
  		updateProduct(id,
  			editProductName,
  			editProductDescription,
  			editProductPrice,
  			editProductCategory,
  			editProductImgUrl
  		)
  	}

	const {_id, imgSrc, category, name, description, price ,isActive} = editProductProp;

	return (
		<Container fluid>
			<Row>
				<Col>
					<Card style={{ width: '20rem', height: '40rem' }} alignment='center' className="text-center border-0 shadow">
						<Card.Img variant="top" src={imgSrc} alt='' />
						<Card.Body style={{ position: "relative" }}>
					   		<Card.Text>Product: {name}</Card.Text>
   							<Card.Text>Description: {description}</Card.Text>
   							<Card.Text>Category: {category}</Card.Text>
						<Card.Subtitle className="mb-2 text-muted">Php {price.toFixed(2)}</Card.Subtitle>

						<div>
							<Row>
								<Col className="col-lg-6 col-sm-12 pr-0">
								{ isActive ?
								    <Button variant="success" onClick={() => modifyStatus(_id,name, "archive")}>Active  </Button>
								: 
								   <Button variant="secondary"  onClick={() => modifyStatus(_id,name, "restore")}>Inactive</Button>
								}
								</Col>
								{" "}
								<Col className="col-lg-6 col-sm-12 pl-0">
								<Button variant="primary"  onClick={() => handleShow(editProductProp)}>Edit Product</Button>
								</Col>
							</Row>
						</div>
						<Modal show={editFormShow} onHide={handleClose}>
						    <Modal.Header closeButton>
						    <Modal.Title>Product Editor</Modal.Title>
						    </Modal.Header>
						    <Modal.Body>
						   		<Form>
						          	<Form.Group controlId="editproductname">
						                <Form.Label>Product Name</Form.Label>
						                <Form.Control 
						                    type="text" 
						                    value={editProductName}
						                    onChange={(e) => setEditProductName(e.target.value)}
						                    required
						                />
						            </Form.Group>
						        </Form>
						        <Form>
						          	<Form.Group controlId="editproductdescription">
						                <Form.Label>Description</Form.Label>
						                <Form.Control 
						                    type="text" 
						                    as="textarea" rows={3} 
						                    value={editProductDescription}
						                    onChange={(e) => setEditProductDescription(e.target.value)}
						                    required
						                />
						            </Form.Group>
						        </Form>
						        <Form>
						          	<Form.Group controlId="editproductprice">
						                <Form.Label>Price</Form.Label>
						                <Form.Control 
						                    type="text" 
						                    value={editProductPrice}
						                    onChange={(e) => setEditProductPrice(e.target.value)}
						                    required
						                />
						            </Form.Group>
						        </Form>

						        <Form>
						          	<Form.Group controlId="editproductcategory">
						                <Form.Label>Category</Form.Label>
						                <Form.Select 
						                    type="text" 
						                    value={editProductCategory}
						                    onChange={(e) => setEditProductCategory(e.target.value)}
						                    required
						                >
						                 <option value=""> </option>
									      <option value="Dog Food">Dog Food</option>
									      <option value="Cat Food">Cat Food</option>
									      <option value="Dog Treats">Dog Treats</option>
									      <option value="Dog Treats">Cat Treats</option>
									      <option value="Pet Grooming Supplies">Pet Grooming Supplies</option>
									      <option value="Pet Health and Wellness">Pet Health and Wellness</option>
									      <option value="Pet Supplies">Pet Supplies</option>
									    </Form.Select>
						            </Form.Group>
						        </Form>

						        <Form>
						          	<Form.Group controlId="editproductimgurl">
						                <Form.Label>Image URL</Form.Label>
						                <Form.Control 
						                    type="text"
						                    as="textarea" rows={3} 
						                    value={editProductImgUrl}
						                    onChange={(e) => setEditProductImgUrl(e.target.value)}
						                    required
						                  />
						            </Form.Group>
						        </Form>
						   </Modal.Body>
						   <Modal.Footer>
						        	<Button variant="secondary" size="sm" onClick={() => setEditFormShow(false)}>Cancel</Button>
						        	<Button variant="primary" size="sm" onClick={() => save(_id)}>Save</Button>
						    </Modal.Footer>
						</Modal> 
				</Card.Body>
			</Card>
			</Col>
			</Row>
		</Container>
	)
}
	
ProductEditor.propTypes = {
    editProductProp: PropTypes.shape({
    	imgSrc: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        category: PropTypes.string.isRequired,
        isActive: PropTypes.bool.isRequired
    })
}