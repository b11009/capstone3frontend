import React from 'react';
import { Link } from 'react-router-dom';
import './Banner.css';
import { Container } from 'react-bootstrap';

export default function Banner({data}){
	console.log(data);
	const {title, content, destination, label} = data;

	return (
		<Container class="d-lg-flex justify-content-center p-md-2">
			<div id="banner" class="home row m-2 mb-4 pb-3">
				<div class="content col-md-12 order-md-2 col-lg-6 order-lg-1">
						<h1>{title}</h1>
						<p>{content}</p>
						<div class="link">
							<Link class="label" to={destination}>{label}</Link>
						</div>
					</div>
				<div class="col-md-12 order-md-1 col-lg-6 order-lg-2 p-1">
					<img src="https://jeniespetstore.s3.ap-southeast-1.amazonaws.com/dogcat.jpeg" alt="" class="landingImg img-fluid"/>
				</div>
			</div>
		</Container>
	)
}