import { Fragment } from 'react';
import { Row, Card, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';


export default function ProductPreview ({productProp}) {

	const {_id, imgSrc, category, name, description, price} = productProp;

	return (
		
		<Card height="100%" style={{ width: '18rem' }} alignment='center' className="text-center border-0 shadow">		
			<Card.Img variant="top" src={imgSrc} alt=''/>
			<Card.Body style={{ position: "relative" }}>
				<Link className="text-decoration-none" to={`/products/details/${_id}`}>{name}</Link>
		   		<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Php {price.toFixed(2)}</Card.Subtitle>
			</Card.Body>
		</Card>
		
	)
}
	
ProductPreview.propTypes = {
    productProp: PropTypes.shape({
    	imgSrc: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        category: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}