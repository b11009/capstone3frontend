
import { Container, Table ,Button} from 'react-bootstrap';
import { Fragment, useEffect, useContext, useState } from 'react';
import { Navigate, useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function userOrder ({order, address, purchasedOn}) {

	// const { user } = useContext(UserContext);

	// const history = useNavigate(); 
	console.log('order.js',order)

	return (
		<Table striped> 
			<thead>
			    <tr>
			          <th>#</th>
			          <th>Product Name</th>
			          <th>Quantity</th>
			          <th>Price</th>
			          <th>Total Amount</th>
			    </tr>
			</thead>
  		        <tbody>
				  {order.items.map((product, idx) => {
			        return (
			        	<tr>
				          <td>{idx+1}</td>
				          <td>{product.productName}</td>
				          <td>{product.quantity}</td>
				          <td>{product.price.toFixed(2)}</td>
				          <td>{(product.price * product.quantity).toFixed(2) }</td>
			        	</tr>)
			      })}
			       	<tr>
			          <td></td>
			          <td>Deliver to: {address}</td>
			          <td>Total Quantity</td>
			          <td></td>
			          <td>Grand Total</td>
			        </tr>
			        <tr>
			          <td></td>
			          <td>Purchased On: {purchasedOn.slice(0,16)}</td>
			          <td>{order.totalQuantity}</td>
			          <td></td>
			          <td>Php {order.totalAmount.toFixed(2)}</td>
			        </tr>
			    </tbody>
		</Table>
	)
}