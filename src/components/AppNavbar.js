import React, { useState,useContext, useEffect, Fragment } from 'react';
import { IconContext } from "react-icons";
import { FaNodeJs } from "react-icons/fa";
import { GiHamburgerMenu } from "react-icons/gi";
import { MdPets, MdClose } from "react-icons/md";
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import {Form, FormControl, Navbar, Nav, NavDropdown, Dropdown, Container, NavItem, NavLink, Button, Uncontrolled} from 'react-bootstrap';

export default function AppNavbar () {

  const { user } = useContext(UserContext);

  return (
     <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" >
        <Container >
          <Navbar.Brand as={Link} to="/" exact className="brandName">
            <img 
              src = "https://jeniespetstore.s3.ap-southeast-1.amazonaws.com/logo.jpg" 
              alt = ""
              width = "75" 
              className = "rounded-circle border border-dark d-inline-block"
            /> Pet Stop
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav"  >
            <Nav className="me-auto">
              <Nav.Link as={Link} to="/" exact>Home</Nav.Link>
              <Nav.Link as={Link} to="/products" exact>Products</Nav.Link>
              
              {/*{user.isAdmin === false ?   
                <Nav.Link as={Link} to="/products" exact>Products</Nav.Link> : <Fragment/>
              }*/}

              {user.isAdmin === true ?
                <NavDropdown
                  title="Admin Dashboard"
                  className="collapsible-nav-dropdown"
                >
                  <NavDropdown.Item as={Link} to="/createProduct" exact>Create Product</NavDropdown.Item>
                  <NavDropdown.Item as={Link} to="/updateProduct" exact>Update Product</NavDropdown.Item>
                  <NavDropdown.Item as={Link} to="/viewAllOrders" exact>All Orders</NavDropdown.Item>
                  <NavDropdown.Item as={Link} to="/logout" exact>Logout</NavDropdown.Item>
                </ NavDropdown>
              : 
                <Fragment/>
              }

              { user.id !== null  && user.isAdmin === false ? 
                <NavDropdown title={user.email.toString()} className="collapsible-nav-dropdown">
                    <NavDropdown.Item as={Link} to="/myCart" exact>My Cart</NavDropdown.Item>
                    <NavDropdown.Item as={Link} to="/myOrder" exact>My Order</NavDropdown.Item>
                    <NavDropdown.Item as={Link} to="/logout" exact>Logout</NavDropdown.Item>
                </NavDropdown>
              :
                <Fragment/>
              }
              
              { user.id === null ?
                <NavDropdown title="My Account" className="collapsible-nav-dropdown">
                  <NavDropdown.Item as={Link} to="/login" exact>Login</NavDropdown.Item>
                  <NavDropdown.Item as={Link} to="/register" exact>Register</NavDropdown.Item>
                </NavDropdown>
              :
                <Fragment/>
              }
              <NavDropdown title="Categories" className=" collapsible-nav-dropdown">
                <NavDropdown.Item as={Link} to="/products" exact>All Categories</NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/products/category/dogfood" exact>Dog Food</NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/products/category/catfood" exact>Cat Food</NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/products/category/dogtreats" exact>Dog Treats</NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/products/category/cattreats" exact>Cat Treats</NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/products/category/petgroomingsupplies" exact>Pet Grooming Supplies</NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/products/category/pethealthandwellness" exact>Pet Health and Wellness</NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/products/category/petsupplies" exact>Pet Supplies</NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>


  )
}