import { Fragment, useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Modal } from 'react-bootstrap';
import { Navigate, useParams, useNavigate, Link } from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductDetails() {

	const { user } = useContext(UserContext);
	const history = useNavigate();
	const { productId } = useParams();

	const [modalCartShow, setModalCartShow] = useState(false);
 	const [modalShow, setModalShow] = useState(false);
 	const handleClose = () => modalShow(false);
  	const handleShow = () => modalShow(true);
	const [productdetails, setProductDetails] = useState("");
	const [address, setAddress] = useState("");
	const [quantity, setQuantity] = useState("");
	const [addToCartQuantity, setAddToCartQuantity] = useState(1)	 	
	 	
	const addToCart = (e, productId) => {
		e.preventDefault(); 
		if (user.id !== null) {

	 	console.log("addToCart")
	 	console.log({
	 				productId: productId,
	 				quantity: addToCartQuantity
	 			})
	 	console.log(localStorage.getItem('token'))
		 	fetch('https://glacial-earth-62899.herokuapp.com/users/addToCart', {
		 			method: "POST",
		 			headers: {
		 				"Content-Type": "application/json",
		 				Authorization: `Bearer ${localStorage.getItem('token')}`
		 			},
		 			body: JSON.stringify([{
		 				productId: productId,
		 				quantity: parseInt(addToCartQuantity)
		 			}])
		 	})
		 	.then(res => res.json())
		 	.then(data => {
		 		console.log("addToCart",data);
		 			if(data){
		 				Swal.fire({
		 					title: "Added to Cart",
		 					icon: "success",
		 					text: "You have successfully added this product to cart."
		 				})
		 				setModalCartShow(false)
		 			}
		 			else{
		 				Swal.fire({
		 					title: "Something went wrong",
		 					icon: "error",
		 					text: "Please try again."
		 				})
		 			}
		 	})
		 } else {
		 	Swal.fire({
						title: "Please log in to order",
						icon: "error",
						text: "Only logged in users can add to cart products"
					})
		 	history("/login");
		 }
	}   
		
	const checkout = (productId) => {
		console.log("checkout", address, quantity)
		if (user.id !== null) {
			fetch('https://glacial-earth-62899.herokuapp.com/users/checkout', {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    "cart": [
				        {
				        "productId": productId,
				        "quantity": quantity
				        }
				    ],
				    "address": address
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				if(data){
					Swal.fire({
						title: "Check out orders",
						icon: "success",
						text: "Check out order Success"
					})
					history("/myOrder");
				}
				else{
					Swal.fire({
						title: "Failed Checkout Order",
						icon: "error",
						text: "Please try again."
					})
				}
			})
		} else {
			Swal.fire({
						title: "Please log in to order",
						icon: "error",
						text: "Only logged in users can order products"
					})
			history("/login");
		}
	}

	useEffect(()=> {
		console.log(productId);
		fetch(`https://glacial-earth-62899.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProductDetails(data);
		})

	}, [productId]);

	return(
		<Container fluid>
			<Row>
				<Col>
					<Card style={{ width: '25rem' }} className="text-center border-0 shadow">
					 	<Card.Img variant="top" src={productdetails.imgSrc} alt='' />
					  	<Card.Body className="text-center">
					    	<Card.Title>{productdetails.name}</Card.Title>
					    	<Card.Subtitle className="mb-2 text-muted">Description:</Card.Subtitle>
					    	<Card.Text>{productdetails.description}</Card.Text>
					    	<Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
					    	<Card.Text>Php {productdetails.price}</Card.Text>
							<Button variant="primary" onClick={() => setModalCartShow(true)} block>Add To Cart</Button>
							<Modal show={modalCartShow} onHide={()=> setModalCartShow(false)}>
					        	<Modal.Header closeButton>
					          		<Modal.Title>Add to Cart Quantity</Modal.Title>
					        	</Modal.Header>
					        	<Modal.Body>
					          	<Form>
					          		<Form.Group controlId="email">
					                	<Form.Label>Quantity</Form.Label>
					                	<Form.Control 
					                    type="number" 
					                    value={addToCartQuantity}
					                    onChange={(e) => setAddToCartQuantity(e.target.value)}
					                    required
					                	/>
					            	</Form.Group>
						            <Form.Group controlId="email">
						                <Form.Label>Total Price</Form.Label>
						                <Form.Control 
						                    type="text" 
						                    value={addToCartQuantity*productdetails.price}
						                    readOnly
						                />
						            </Form.Group>
					          	</Form>
					        	</Modal.Body>
					        	<Modal.Footer>
					          		<Button variant="secondary" onClick={() => setModalCartShow(false)}>
					            	Cancel</Button>
						        	{addToCartQuantity >= 1?  
						        		<Button variant="primary" onClick={(e) => addToCart(e, productdetails._id)}>
						            	Add To Cart</Button>
						        	:
						          		<Button variant="primary" onClick={() => setModalShow(false)} disabled>
						            	Add To Cart</Button>
						      		}
						        </Modal.Footer>
					     	</Modal> 
						 	{" "}
							<Button variant="primary" onClick={() => setModalShow(true)} block>Checkout</Button>
							<Modal show={modalShow} onHide={handleClose}>
						        <Modal.Header closeButton>
						          	<Modal.Title>Order Confirmation</Modal.Title>
						        </Modal.Header>
						        <Modal.Body>
						          	<Form>
						          		<Form.Group controlId="email">
						                	<Form.Label>Quantity</Form.Label>
						                	<Form.Control 
						                    type="number" 
						                    value={quantity}
						                    onChange={(e) => setQuantity(e.target.value)}
						                    required
						                	/>
						            	</Form.Group>
							            <Form.Group
							              	className="mb-3"
							              	controlId="exampleForm.ControlTextarea1"
							              	value={address}
			                    		  	onChange={(e) => setAddress(e.target.value)}
							              	required
							            >
							              	<Form.Label>Delivery Address</Form.Label>
							              	<Form.Control as="textarea" rows={3} />
							            </Form.Group>
						          	</Form>
						        </Modal.Body>
							    <Modal.Footer>
							        <Button variant="secondary" onClick={() => setModalShow(false)}>
							        Cancel</Button>
							        { address !== "" && quantity >= 1?  
							        <Button variant="primary" onClick={() => checkout(productdetails._id)}>
							        Confirm Checkout</Button>
							        :
							        <Button variant="primary" onClick={() => setModalShow(false)} disabled>
							        Confirm Checkout</Button>
							      	}
							    </Modal.Footer>
						     </Modal> 
					  	</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}