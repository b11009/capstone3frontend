import { Fragment } from 'react';
import { Row, Card, Col, Carousel, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Spinner from 'react-bootstrap/Spinner';

export default function ProductCarousel ({productProp}) {

	console.log("data.slice(0,2)",productProp)
	console.log("typeof", typeof productProp)
	console.log("productProp.length", productProp.length)

	return ( 
		<Container>
			<Row className="justify-content-center">
				<Col className="col-lg-6 col-sm-12">
					<h1 className="hotProducts pt-3">Hot Products</h1>
					
					{productProp.length == 0 ?
						<Spinner animation="border" />
						:
						<Carousel fade alignment='center' className="text-center border-0 shadow"  variant="dark">
							{productProp.map(({productId, imgSrc, category, productName, description})=>{
								return (
								<Carousel.Item key={productId}>
									<img
									className="d-block w-100 m-auto"
									src={imgSrc}
									alt="First slide"
									/>
									
									<Carousel.Caption className="py-3">
									    <Link className="text-decoration-none" to={`/products/details/${productId}`}><h3>{productName}</h3></Link>
									        <p>{description}</p>
									</Carousel.Caption>
									
								</Carousel.Item>
								)
							})}
						</Carousel>
							
					}
				</Col>
			</Row>
		</Container>
	)
	
}
