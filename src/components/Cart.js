
import { Container, Table ,Button , Modal} from 'react-bootstrap';
import { Fragment, useEffect, useContext, useState } from 'react';
import { Navigate, useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Form from 'react-bootstrap/Form';

import Swal from 'sweetalert2';

export default function Cart ({cart ,updateCart, checkoutCart}) {

	const { user } = useContext(UserContext);
	const [checkoutCartModel, setcheckouCartModal] = useState(false);
	const [deliveryAddress, setDeliveryAddress] = useState('')
	const history = useNavigate(); 
	console.log({cart})
	return (
		<Table>
			<thead>
			    <tr>
			        <th>#</th>
			        <th>Product Name</th>
			        <th>Quantity</th>
			        <th>Price</th>
			        <th>SubTotal</th>
			        <th></th>
			    </tr>
			</thead>
  		    <tbody>
			{cart.items.map((product,idx) => {
			    return (
			        <tr>
			            <td>{idx+1}</td>
			         	<td>{product.productName}</td>
			          	<td>
			          		<Button variant="primary" onClick={() => updateCart(product.productId, -1)} block>-</Button>
			            	{" " + product.quantity+ " "} 
			          		<Button variant="primary" onClick={() => updateCart(product.productId, 1)} block>+</Button>
					  	</td>
			          	<td>{product.price}</td>
			          	<td>{product.price * product.quantity }</td>
			          	<td> <Button variant="danger" onClick={() => updateCart(product.productId, -1 * product.quantity)} block>X</Button> </td>
			        </tr>
			    )
			})}
			       <tr>
			          	<td></td>
			          	<td> 
				          	{cart.items.length > 0?
				          		<Button key="checkoutCarKey" variant="primary" onClick={() => setcheckouCartModal(true)}>Checkout Cart</Button>
					     		:
					      		<Button key="checkoutCarKey" variant="primary" disabled>Checkout Cart</Button>
					  		}
					  		<Modal show={checkoutCartModel} onHide={()=> setcheckouCartModal(false)}>
					        	<Modal.Header closeButton>
					          		<Modal.Title>Checkout Cart Confirmation</Modal.Title>
					       		</Modal.Header>
						        <Modal.Body>
						          	<Form>
						            	<Form.Group
						            	className="mb-3"
						              	controlId="exampleForm.ControlTextarea1"
						              	value={deliveryAddress}
		                    		  	onChange={(e) => setDeliveryAddress(e.target.value)}
						              	required
						            	>
						              	<Form.Label>Delivery Address</Form.Label>
						              	<Form.Control as="textarea" rows={3} />
						            	</Form.Group>
						          	</Form>
						        </Modal.Body>
						        <Modal.Footer>
						          	<Button variant="secondary" onClick={() => setcheckouCartModal(false)}>
						            	Cancel
						          	</Button>
						        	{ deliveryAddress !== "" ?  
						        		<Button variant="primary" onClick={(e) => checkoutCart(e,cart.items, deliveryAddress)}>Confirm Checkout</Button>
						        	:
						          		<Button variant="primary" onClick={() => setcheckouCartModal(false)} disabled>Confirm Checkout</Button>
						      		}
						        </Modal.Footer>
					    	</Modal> 
				      	</td>
				        <td>{cart.totalQuantity}</td>
				        <td>Total</td>
				        <td>{cart.totalAmount}</td>
			       </tr>
			</tbody>
		</Table>
	)
}